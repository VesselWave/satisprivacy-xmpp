# SatisPrivacy-XMPP

[SatisPrivacy](https://satisprivacy.org) is an ejabberd instance that tries to be up to date with best possible experience. Most of it is based on [LavaTech / a3.pm](https://gitlab.com/lavatech/a3.pm) so HUGE thanks to them

[![Compliance tool badge](https://compliance.conversations.im/badge/satisprivacy.org/)](https://compliance.conversations.im/server/satisprivacy.org)

---

## Support

[admin@satisprivacy.org](xmpp:admin@satisprivacy.org) on XMPP or an email [vesselwave@protonmail.com](mailto:vesselwave@protonmail.com)

## Installation

Yes, you can make your own XMPP server based on this configurations

##### Requirements

- Some linux server, not under a NAT if you want to make audio/video calls

- Domain name with ability to make subdomains

### Ejabberd

Get ejabberd [https://docs.ejabberd.im/admin/installation/](https://docs.ejabberd.im/admin/installation/) from it's developers.

OR

Build yourself

I am using Ejabberd 22.05. I build it from source as [a3.pm](https://gitlab.com/lavatech/a3.pm#ejabberd-version) so checkout their command to build and install ejabberd. [Installing ejabberd | ejabberd Docs](https://docs.ejabberd.im/admin/installation/#source-code)

```bash
make distclean && ./autogen.sh && ./configure --disable-elixir --enable-pgsql && make && sudo make install && sudo chmod +x /usr/local/sbin/ejabberdctl && sudo chown ejabberd:ejabberd /usr/local/sbin/ejabberdctl && sudo systemctl restart ejabberd
```

### Automatic Let's Encrypt certificate renewal

Here I recomend [Anton Putra's](https://youtu.be/7jEzioFsyNo) guide for [acme-dns](https://github.com/joohoi/acme-dns), [acme-dns-client](https://github.com/acme-dns/acme-dns-client) and [certbot]() to automate issuing wildcard certficates

###### Other options

- [ejabberd ACME](https://docs.ejabberd.im/admin/configuration/basic/#acme)

- Certbot and list of ejabberd's subdomains. Default list:
  
  - pubsub.example.com
  
  - proxy.example.com
  
  - conference.example.com
  
  - upload.example.com

- Manually renew certificate every 60-90 days *(not recomended)*

### Nginx

Just install, modify configuration to match your needs and put it in nginx directory. Most often it is `/etc/nginx/`

### PostgreSQL

SatisPrivacy uses [default](https://docs.ejabberd.im/admin/configuration/database/#default-and-new-schemas) ejabberd database scheme

### S2S spam protection

SatisPrivacy follows latest [jabberSPAM's server blacklist](https://github.com/JabberSPAM/blacklist) by disabling federation with those servers. I recommend keeping it

### Correct permissions for websites in linux:

https://www.getpagespeed.com/server-setup/nginx-and-php-fpm-what-my-permissions-should-be

## Licenses

- The config is released under GPLv2 as the config file I'm basing on ([a3.pm's](https://gitlab.com/lavatech/a3.pm/-/blob/master/ejabberd.yml)) is using that license.
- The `dns-records` file is released to public domain

Under `website/`:

- The `conversejs.html` file is released under GPLv2 license, by @a.
- The `xmpp.html`, `xmpp.ru.html`, `info.html`,`info.ru.html` and `satisprivacy-xmpp.css` files are released under GPLv2, by @slice and @vesselwave.
- The files under `.well-known` are released to public domain, by @a and @luna.
